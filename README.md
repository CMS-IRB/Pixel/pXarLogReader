# pXar Log Reader

## Prerequisites

Install postfix to be able to receive system emails
```
sudo apt-get install postfix
```
Configuration in
```
/etc/postfix/main.cf
```
Edit `/etc/aliases` and add an email or comma separated list of emails for the root account
```
root: email@example.com
```
and run
```
sudo newaliases
```
Restart the postfix service
```
sudo service postfix restart
```

Install monit
```
sudo apt-get install monit
```
Configuration in
```
/etc/monit/monitrc
```
Important lines
```
set mailserver localhost

set httpd port 2812 and
    use address localhost  # only accept connection from localhost
    allow localhost        # allow localhost to connect to the server and
```
Check the monit configuration
```
sudo monit -t
```
Reload the monit service
```
sudo monit reload
```
or
```
sudo service monit restart
```
Specific checks can be started/stopped by running
```
sudo monit start/stop <check>
```

## Install pXar Log Reader

Install Python psutil module
```
sudo apt-get install python-psutil
```
Get the necessary code from GitLab by running the following
command from the home folder
```
git clone https://gitlab.cern.ch/CMS-IRB/Pixel/pXarLogReader.git PixelTools
```
This step needs to be done only once.

Next, check that the paths defined in lines 8 and 10 in `PixelTools/pXar_log_reader`
are correct (currently defined for user 'pixel'). Check line 12 in
`PixelTools/pXar_log_reader` and update the module configuration to be used
(currently M1012). Also check line 41 in `PixelTools/pXar_log_reader.py`
to make sure that all desired pXar options are properly set.

Now go to the pXar Python folder
```
cd pxar/python
```
and create a link to the pXar_log_reader script
```
ln -s /home/pixel/PixelTools/pXar_log_reader.py pXar_log_reader.py
```
Next, open Monit configuration file `/etc/monit/monitrc`
and change the check interval to 5 seconds
```
set daemon 5
```
and add the following lines (here for user 'pixel', change accordingly) in the section for checking system processes
```
check process pxar with pidfile /tmp/pXar_log_reader.pid
   alert email@example.com only on { nonexist }
   start program = "/home/pixel/PixelTools/pXar_log_reader start"
        as uid pixel and gid pixel
   stop program = "/home/pixel/PixelTools/pXar_log_reader stop"
        as uid pixel and gid pixel
```
Save and close the file.

Check the monit configuration
```
sudo monit -t
```
and reload the monit service
```
sudo monit reload
```
You can now open the Monit management page by pointing your browser to

localhost:2812

and from there you can start/stop the pxar process. The log file will be written
to `pxar/python/test_beam.log`. The pXar `.root` and `.log` files will be written to
the usual location inside the module configuration folder.
