#!/usr/bin/python

import os
import sys
from optparse import OptionParser
import logging
import time
import re
import subprocess
import psutil
import signal
import collections
import copy


def writePidFile(basename):
    pid = str(os.getpid())
    pidFilename = '/tmp/%s.pid' % basename.rstrip('.py')
    if os.path.exists(pidFilename):
        os.remove(pidFilename)
    pidFile = open(pidFilename, 'w')
    pidFile.write(pid)
    pidFile.close()


class TimeoutException(Exception):   # Custom exception class
    pass


def timeout_handler(signum, frame):  # Custom signal handler
    raise TimeoutException


class PXarProcess():

    def __init__(self, logger, options):
        self._powercycle = False
        self._restart    = False
        self._stack      = collections.defaultdict(dict)
        self.counter     = 0
        self.logger      = logger
        self.options     = options
        self.logger.debug('Starting pXar subprocess')
        self.proc = subprocess.Popen(["../bin/pXar", "-d", self.options.dir, "-nomemreset"],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            preexec_fn=os.setpgrp) # prevents forwarding any signals from the main process
                                   # to allow pXar to finish running tests gracefully
        self.logger.debug('pXar subprocess started')
        time.sleep(1)
        # position the process output reader at the end of the startup section of the process log
        self.read_output('INFO: enter test to run')
        self.clear_stack()

    def clear_stack(self):
        self.send_command('cmd:start\n')
        self.read_output('INFO: enter test to run')
        # clear the initial stack state by writing something into it
        self.send_command('cmd:seq_15\n')
        self.read_output('INFO: enter test to run')
        self.send_command('cmd:loop\n')
        self.read_output('INFO: enter test to run')
        # let it loop for a bit
        time.sleep(2)
        self.send_command('cmd:stop\n')
        self.read_output('INFO: enter test to run')
        
    def empty_stack(self):
        self._stack.clear()

    def request_powercycle(self):
        self._powercycle = True

    def should_powercycle(self):
        return self._powercycle

    def clear_powercycle(self):
        self._powercycle = False

    def powercycle(self):
        self.logger.info('Starting DTB powercycle')
        self.logger.info('  Turning HV off...')
        self.send_command('IV:hvoff\n')
        self.read_output('INFO: enter test to run')
        self.logger.info('  HV turned off')
        time.sleep(2)
        self.logger.info('  Turning LV off...')
        self.send_command('IV:poweroff\n')
        self.read_output('INFO: enter test to run')
        self.logger.info('  LV turned off')
        time.sleep(2)
        self.logger.info('  Turning LV on...')
        self.send_command('IV:poweron\n')
        self.read_output('INFO: enter test to run')
        self.logger.info('  LV turned on')
        time.sleep(2)
        self.logger.info('  Turning HV on...')
        self.send_command('IV:hvon\n')
        self.read_output('INFO: enter test to run')
        self.logger.info('  HV turned on')
        self.logger.info('DTB powercycle done')
        self.clear_powercycle()
        self.clear_stack()
        self.send_command('cmd:programdut\n')
        self.read_output('INFO: enter test to run')

    def stop(self):
        self.logger.debug('Terminating pXar subprocess')
        self.proc.communicate('q')
        #self.proc.terminate()
        self.logger.debug('pXar subprocess terminated')

    def request_restart(self):
        self._restart = True

    def should_restart(self):
        return self._restart

    def restart(self):
        self.logger.info('Stopping pXar')
        self.stop()
        self.logger.info('Restarting pXar')
        self.__init__(self.logger, self.options)

    def send_command(self, cmd):
        try:
            self.proc.stdin.write(cmd)
            self.proc.stdin.flush()
        except IOError:
            self.logger.info('pXar IOError encountered. pXar possibly crashed and will be restarted')
            self.request_restart()

    def read_output(self, match, timeout=5, lookForSEU=False):
        # change the behavior of SIGALRM
        old_handler = signal.signal(signal.SIGALRM, timeout_handler)

        readStack = False
        readStackError = False
        stack = collections.defaultdict(dict)

        while True:
            # set the timeot for each line read
            signal.alarm(timeout)

            try:
                self.proc.stdout.flush()
                line = self.proc.stdout.readline()

            except TimeoutException:
                self.logger.warning('Reading pXar output timed out before finding a matching string. pXar possibly crashed and will be restarted')
                self.request_restart()

            signal.alarm(0)  # alarm removed

            # in case pXar output closed unexpectedly
            if line.strip() == '':
                #self.logger.warning('pXar output closed unexpectedly before finding a matching string. pXar possibly crashed and will be restarted')
                #self.request_restart()
                #break
                pass
            else:
                self.logger.debug('=== pXar output: ' + line.strip())

                if "Could not find any connected DTB." in line:
                    self.logger.error('Critical error: Could not find any connected DTBs. Will now pause until interrupted')
                    signal.pause()

            if lookForSEU:
                # look for readout problems
                if 'N_ROC_HEADER' in line:
                    n_roc_header = [int(n) for n in line.split('N_ROC_HEADER')[1].split()]
                    if 0 in n_roc_header:
                        self.logger.info('TBM SEU candidate detected during readout test %i' % self.counter)
                        self.logger.info(line.split('INFO: ')[1].strip())
                        if not self.should_powercycle():
                            self.logger.info('Requesting DTB powercycle')
                            self.request_powercycle()
                        else:
                            self.logger.info('DTB powercycle already requested')
                # read stack
                if 'INFO: core             0 A' in line:
                    readStack = True
                if readStack:
                    if self.should_powercycle():
                        self.logger.info(line.split('INFO: ')[1].rstrip() if ('INFO: ' in line) else line.rstrip())
                    # read lines with the right pattern
                    if re.search('^\d*\)', line.lstrip()):
                        if 'err' in line:
                            readStackError = True
                        line_tmp = line.strip().split("   ")
                        line_l = []
                        for el in line_tmp:
                            if el == '':
                                continue
                            else:
                                line_l.append(el.strip())
                        # store info for all 4 TBM cores
                        stack['0 A'][line_l[0]] = (line_l[1].split()[1] if len(line_l[1].split())==2 else 'err')
                        stack['0 B'][line_l[0]] = (line_l[2].split()[1] if len(line_l[2].split())==2 else 'err')
                        stack['1 A'][line_l[0]] = (line_l[3].split()[1] if len(line_l[3].split())==2 else 'err')
                        stack['1 B'][line_l[0]] = (line_l[4].split()[1] if len(line_l[4].split())==2 else 'err')
                    if line.lstrip().startswith('31)'):
                        readStack = False

            # check if the line contains what we are looking for
            if match in line:
                break

        signal.signal(signal.SIGALRM, old_handler)  # old signal handler restored
        signal.alarm(0)  # alarm removed
        
        if readStackError:
            if (self.counter < self.options.report and self.counter%self.options.report == 1) or self.counter%self.options.report == 0:
                self.logger.error('Error reading the TBM stack')
            else:
                self.logger.debug('Error reading the TBM stack')
        
        if lookForSEU and self.should_powercycle() and len(stack) > 0:
            # if stack state already stored
            if len(self._stack) > 0:
                stuckCores = []
                # loop over TBM cores
                for key1 in stack.keys():
                    stuckCore = True
                    # loop over counts
                    for key2 in stack[key1].keys():
                        if stack[key1][key2] != self._stack[key1][key2] or (stack[key1][key2] == 'err' or self._stack[key1][key2] == 'err'):
                            stuckCore = False
                    if stuckCore:
                        stuckCores.append(key1)
                if len(stuckCores) > 0:
                    self.logger.info('---> Real TBM SEU detected for core(s) %s during readout test %i' % (str(stuckCores)[1:-1], self.counter) )
                self.empty_stack()
            # store stack state
            else:
                self._stack = copy.deepcopy(stack)


class ExitHandler():

    def __init__(self, pxar, logger):
        self.pxar   = pxar
        self.logger = logger

    def exit(self):
        self.logger.info('Program interrupted. Stopping pXar')
        # send the stop signal
        self.pxar.stop()
        self.logger.info('Exiting main program')
        sys.exit(0)

    def catch_sigterm(self, signal, frame):
        self.exit()


def main():
    # usage description
    usage = "Usage: python %prog [options] \nExample: python %prog -d ../main/M1012/"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-d", "--dir", dest="dir",
                        help="Directory containing module or ROC configuration",
                        metavar="DIR")

    parser.add_option("-r", "--report", dest="report",
                        help="Report every N readout tests",
                        type='int',
                        default=10,
                        metavar="N")
    
    parser.add_option("-s", "--simulate", dest="simulate",
                        help="Simulate SEU every N readout tests",
                        type='int',
                        metavar="N")
    
    parser.add_option("-v", "--verbose", dest="verbose",
                        help="Verbose mode",
                        action="store_true",
                        default=False)

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not options.dir:
        parser.print_usage()
        sys.exit(1)

    # write a PID file which can be used for process monitoring (e.g. using Monit)
    writePidFile( os.path.basename(sys.argv[0]) )

    # set up the logger
    formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    log_file = 'test_beam.log'
    handler = logging.FileHandler(log_file, mode='a')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = logging.getLogger('test_beam_logger')
    logger.setLevel(logging.DEBUG if options.verbose else logging.INFO)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)

    logger.info('Starting main program')

    # before starting the pXar thread, check for any leftover pXar processes which need to be killed
    for proc in psutil.process_iter():
        # check whether the process name matches
        pdict = proc.as_dict(attrs=['name'])
        if pdict['name'] == 'pXar':
            proc.kill()

    # start the pXar process
    logger.info('Starting pXar')
    pxar = PXarProcess(logger, options)

    # initialize the exit handler
    exit_handler = ExitHandler(pxar, logger)

    # for catching kill signal
    signal.signal(signal.SIGTERM, exit_handler.catch_sigterm)

    try:
        firstRun = True

        while True:
            # if needed, restart pXar
            if pxar.should_restart():
                pxar.restart()
            # if requested, powercycle DTB
            if pxar.should_powercycle():
                pxar.powercycle()
            # make a short pause before running the test in case powercycle/restart/stop have been requested
            if not firstRun:
                time.sleep(2)
            # run the test
            if not ( pxar.should_restart() or pxar.should_powercycle() ):
                firstRun = False
                pxar.counter += 1
                if (pxar.counter < options.report and pxar.counter%options.report == 1) or pxar.counter%options.report == 0:
                    logger.info('Running readout test %i' % pxar.counter)
                logger.debug('Readout test: %i' % pxar.counter)
                # simulate a TBM SEU
                if options.simulate and pxar.counter%options.simulate == 0:
                    pxar.send_command('cmd:do_0:100_[freeze_1]\n')
                    pxar.read_output('INFO: enter test to run', timeout=30)
                pxar.send_command('cmd:test_freeze\n')
                pxar.read_output('INFO: enter test to run', lookForSEU=True)
                if pxar.counter%1 == 0 or pxar.should_powercycle():
                    pxar.send_command('cmd:read_stack\n')
                    pxar.read_output('INFO: enter test to run', lookForSEU=True)
                if pxar.should_powercycle():
                    pxar.send_command('cmd:test_freeze\n')
                    pxar.read_output('INFO: enter test to run', lookForSEU=True)
                    pxar.send_command('cmd:read_stack\n')
                    pxar.read_output('INFO: enter test to run', lookForSEU=True)
                pxar.logger.debug('Readout test done')

            # quit the program using the stop file
            if os.path.exists("stop"):
                print "Want to stop now"
                os.remove("stop")
                exit_handler.exit()

    except KeyboardInterrupt:
        exit_handler.exit()


if __name__ == '__main__':
    main()
